# The Valuation of Interest Rate Swap with Bilateral Counterparty Risk

Interest rate swap (IRS) is one of the most popular financial derivatives. In the market, IRS’s are quoted irrespective of credit ratings of counterparties. In another words, they are considered as default-free. The valuation of default-free IRS’s under a term structure of interest rates is a classical exercise. However, swap contracts are traded over-the-counter (OTC) and are not backed by the guarantee of a clearing corporation or an exchange. As a consequence, each party is exposed to the credit risk (default risk). Historical experience shows that credit risk often leads to significant losses. Therefore, one should incorporate the cost of the counterparty risk into the swap price. Also, regulatory issues related to the Basel II framework encourage the inclusion of default risk into valuation.

Pricing defaultable derivatives or pricing the counterparty credit risk is a relatively new area of derivatives modeling and trading. Credit value adjustment (CVA) allows us to quantify counterparty credit risk as a single, measurable Profit & Loss number. By definition, CVA is the difference between the risk-free trade value and the true (or risky or defaultable) trade value that takes into account the possibility of counterparty’s default. Commonly, the CVA is not paid as a lump-sum upfront premium, but rather is structured into a funding spread. The risk-free trade value is what brokers quote or what trading systems or models normally report. The defaultable trade value, however, is a relatively less explored and less transparent area, which is the main challenge and core theme for credit risk adjustment (see Xiao [2015], [2017]).

IRS is a typical bilateral contingent contract that can be either an asset or a liability to each party during the life of the contract. Unlike the unilateral defaultable claim valuation problems that have been studied extensively by many authors, the valuation of bilateral contingent claims is still lacking convincing mechanism. The problem is mainly caused by the asymmetric credit qualities and the asymmetric default settlement rules.

Sundaresan (1991), Longstaff and Schwartz (1993), and Tang and Li (2007) simplify the problem by considering the IRS as a simple exchange of loans (receivable parts and payable parts). It is inappropriate to value a defaultable IRS by pricing the default risk of the promised gross payment from each party separately and then adding the two together. Because the promised cash flow exchange in an IRS is always netted. Another simplification consists of taking into account the presence of one risky counterparty only, as in Li (1998) and Arvanities and Gregory (2001). These approaches overlook the presence of bilateral default risk.

The first study on asymmetric defaultable IRS is conducted by Duffie and Huang (1996). They use a short rate interest rate model combined with a reduced-form default model and lead to numerical approximations by solving a recursive integral. Even the authors admit that it is a substantial complexity solution. Hubner (2001) extends the work carried out by Duffie and Huang (1996) and gets a closed-form solution by introducing a one-dimensional state variable X that can be thought of as a ratio of the market value of the firm’s assets. The author, however, does not show how to calibrate the state variable and not even provide a simple example. In general, these proposed models are not practical enough to use.

In this paper, we present an analytical model for valuing contingent claims subject to default by both parties. While the general principles of the valuation model can be applied to other bilateral defaultable contingent claims, we focus on the valuation of defaultable IRS’s in which both parties are exposed to credit risk. The approach is based on market models for interest rates and a reduced-form model for the default time. All quantities modeled are market-observable. With the closed-form solution, we can analyze the impact of credit risk on swap value, swap rate, swap spread and CVA more closely. We confirm the results in Duffie and Huang (1996) and also report some new findings.

The object modeled under the market models is risk-observable. It is also consistent with the market standard approach for pricing caps/floors using Black’s formula. The market models have now become some of the most popular models for pricing such derivatives. They are generally considered to have more desirable theoretical calibration properties than short rate or instantaneous forward rate models.

Unlike structural models, reduced-form models do not condition default explicitly on the value of the firm, and parameters related to the firm’s value need not be estimated to implement the model. For pricing and hedging, reduced-form models are the preferred methodology.

Our closed-form solution shows that the value of a bilateral defaultable IRS is the sum of the values of individual bilateral defaultable swaplets. Each bilateral defaultable swaplet can be replicated by buying a risk-adjusted call option and selling a risk-adjusted put option. The risk-adjusting factors depend on hazard rates, recovery rates and settlement rules.

In the case where the floating-rate payer is a LIBOR party, we confirm findings in Duffie and Huang (1996) that the swap spreads are relatively less sensitive to credit quality comparing to the bond spreads or the CDS spreads. The credit impact on swap rates is approximately linear. In the case where both parties have spreads against the LIBOR that was not studied closely before, we find that the credit impact on swap rates is not linear any more. The swap funding spreads have a significantly impact on swap spreads as well.

Reference

ArVanities, A., and Gregory, J., 2001, Credit: the complete guide to pricing, hedging and risk management, Risk Books.

Duffie, D. and Huang, M., 1996, Swap rates and credit quality, Journal of Finance, 51, 921-949.

Duffie, D. and Singleton, K.J., 1997, An econometric model of the term structure of interest rate swap yields, Journal of Finance, 52, 1287-1321

FinPricing, 2019, Data Analytics, https://finpricing.com/lib/IrCurveIntroduction.html

Huge, B. and Lando, D., 1999, Swap pricing with two-sided default risk in a rating-based model, European Finance Review, 3, 239-268.

Hubner, G., 2001, The analytic pricing of asymmetric defaultable swaps, Journal of Banking & Finance, 25, 295-316

Jarrow, R. and Turnbull, S., 1995, Pricing options on financial securities subject to default risk, Journal of Finance, 50, 53-86.

Li, H., 1998, Pricing of swaps with default risk, Review of Derivatives Research, 2, 231-250.

Longstaff, F.A. and Schwartz, E.S., 1993, Valuing risky debt: a new approach, working paper, The Anderson Graduate School of Management, UCLA.

Rendleman, R.J., 1992, How risks are shared in interest rate swaps, Journal of Financial Services Research, 7, 5-34.

Sundarean, S., 1991, Valuation of swaps, in Recent Developments in International Banking and Finance, S. Khoury, ed. Amsterdam: North Holland.

Tang, Y. and Li, B., 2007, Quantitative analysis, derivatives modeling, and trading strategies, World Scientific Publishing.

Xiao, T., 2017, “A New Model for Pricing Collateralized OTC Derivatives.” Journal of Derivatives, 24(4), 8-20.

Xiao, T., 2015, “An Accurate Solution for Credit Valuation Adjustment (CVA) and Wrong Way Risk.” Journal of Fixed Income, 25(1), 84-95.

